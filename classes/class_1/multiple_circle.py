import turtle as t

s = t.getscreen()

s.bgcolor("light green")

t.speed(0)
for _ in range(36):
    t.left(10)
    t.circle(100)

t.done()
