# Python for Kids - Class 1

_TheWalker - Online Coding Classes_

_Trainer : R Diwakar C_

Link:

https://bitbucket.org/Diwakar009/pythononlineclasses/src/master/classes/class_1/topic_material.md

Batch 1 Material.

https://bitbucket.org/Diwakar009/pythononlineclasses/src/master/classes/class_1/topic_material_Batch1.md

Batch 2 Material.

https://bitbucket.org/Diwakar009/pythononlineclasses/src/master/classes/class_1/topic_material_Batch2.md



## Class 1 Covered:

Introduction to Python.

Using python.

Running First Program

## Class 2 Covered:

First Drawing with Turtle Module

## Class 3 Covered:

Calculation's and Arithmetic Operator's

Knowing about Variable's


## Pending Documentation:

Condition's : If, Else If, Else

Loop's : For Loop

Loop's : While Loop
        
Data type : List Variables

Data type : Tuple Variables

Data type : Maps Variables (Dictionary)



