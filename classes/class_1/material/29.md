## Working with files

Creating a Test File

We’ll experiment with a text file we’ll call test.txt. Follow the steps
for the operating system you’re using.

Creating a New File in Windows

If you’re using Windows, follow these steps to create test.txt:
        
        1. Select Start4All Programs4Accessories4Notepad.
        
        2. Enter a few lines into the empty file.
        
        3. Select File4Save.
        
        4. When the dialog appears, select the C: drive by double-clicking
        
        My Computer and then double-clicking Local Disk (C:).
        
        5. Enter test.txt in the File name box at the bottom of the dialog.
        
        6. Finally, click the Save button.

Creating a New File in Mac OS X

If you’re using a Mac, follow these steps to create test.txt:

        1. Click the Spotlight icon in the menu bar at the top of the screen.
        
        2. Enter TextEdit in the search box that appears
        
        TextEdit should appear in the Applications section. Click it to
        open the editor (you can also find TextEdit in the Applications
        folder in Finder).
        4. Type a few lines of text into the empty file.
        5. Select Format4Make Plain Text.
        6. Select File4Save.
        7. In the Save As box, enter test.txt.
        8. In the Places list, click your username—the name you logged
        in with or the name of the person who owns the computer
        you’re using.
        9. Finally, click the Save button.
        Creating
        
### Opening a File in Python

Python’s built-in open function opens a file in the Python shell and displays its contents. How you tell the function which file to open
depends on your operating system. Look over the example for a Windows file, and then read the Mac- or Ubuntu-specific section
if you’re using one of those systems.

Opening a Windows File

If you’re using Windows, enter the following code to open test.txt:

        >>> test_file = open('c:\\test.txt')
        >>> text = test_file.read()
        >>> print(text)
        There once was a boy named Marcelo
        Who dreamed he ate a marshmallow
        He awoke with a start
        As his bed fell apart
        And he found he was a much rounder fellow

On the first line, we use open, which returns a file object with functions for working with files. The parameter we use with the
open function is a string telling Python where to find the file. If you’re using Windows, you saved test.txt to the local disk on the C:
drive, so you specify the location of your file as c:\\test.txt.

The two backslashes in the Windows filename tell Python that the backslash is just that, and not some sort of command. (As
you learned in Chapter 3, backslashes on their own have a special meaning in Python, particularly in strings.) We save the file object
to the variable test_file. 

On the second line, we use the read function, provided by the file object, to read the contents of the file and store it in the variable
text. We print the variable on the final line to display the contents of the file.

Opening a Mac OS X File

If you are using Mac OS X, you’ll need to enter a different location on the first line of the Windows example to open test.txt. Use
the username you clicked when saving the text file in the string. For example, if the username is sarahwinters, the open parameter
should look like this:

        >>> test_file = open('/Users/vikram/test.txt')
             
        
## Writing to Files

The file object returned by open has other functions besides read.
We can create a new, empty file by using a second parameter, the
string 'w', when we call the function:

        >>> test_file = open('c:\\myfile.txt', 'w')
        
The parameter 'w' tells Python that we want to write to the
file object, rather than read from it.

We can now add information to this new file using the write
function:

        >>> test_file = open('c:\\myfile.txt', 'w')
        >>> test_file.write('this is my test file')
        
Finally, we need to tell Python when we’re finished writing to
the file, using the close function:

        >>> test_file = open('c:\\myfile.txt', 'w')
        >>> test_file.write('What is green and loud? A froghorn!')
        >>> test_file.close()
        
Now, if you open the file with your text editor, you should see
that it contains the text “What is green
and loud? A froghorn!” Or, you can use
Python to read it again:

        >>> test_file = open('myfile.txt')
        >>> print(test_file.read())
        What is green and loud? A froghorn!

## Appending to files

        test_file = open('c:\\myfile.txt', 'a')
        test_file.write("Appended String")
        test_file.close()
        
        test_file = open('c:\\myfile.txt', 'r')
        file_data = test_file.read()
        print(file_data)
        test_file.close()

# Try the Sequence of steps in IDLE

        >>> file = open('sample.txt','w')
        >>> file.write('Hello how r u')
        >>> file.close()
        >>> file = open('sample.txt','r')
        >>> file.read()
        'Hello how r u'
        >>> file.close()
        >>> file = open('sample.txt','a')
        >>> file.write(', Where are you now a days')
        >>> file.close()
        >>> file = open('sample.txt','r')
        >>> file.read()
        'Hello how r u, Where are you now a days'
        

 ![alt text](img/119.PNG)       
        

