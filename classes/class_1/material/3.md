## Calculation's and Arithmetic Operator's

Python Shell can be used as normal calculator, follow the commands from bellow image.

![alt text](img/4.PNG)

## Python Arithmetic Operator's

|Operator|Description|Sample Operation|
|---|---|---|
|+|	Addition|	2 + 2 = 3|
|-|	Subtraction|	4 – 2 = 2|
|*|	Multiplication|	2 * 2 = 4|
|/|	Division|	4 /2 = 2|
|%|	Modulus (Returns Remainder)|	4 % 2 = 0|
|//|	Floor Division (Omits decimal part)	|5 // 2 = 2 |
|**|	Exponent or Power|	2 ** 2 = 4|

Order of Operation

Operation is anything that uses an operator’s

Multiplication and division have a higher order than addition and Subtraction, 
which means that they’re performed first.

But if any operation inside the parenthesis will get executed first then higher order operator's.

![alt text](img/5.PNG)
