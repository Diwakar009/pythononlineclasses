# Kids Python Topic Material

## Index

[1. Introduction to Python](./1.md)

[2. Using python](./1.md)

[3. Running First Program](./2.md)

[4. First Drawing with Turtle Module](./2.md)

[5. Calculation's and Arithmetic Operator's](./3.md)

[6. Knowing about Variable's](./4.md)

[7. Exercises](./5.md)

[8. Numeric Datatype: Integer and Float Datatype](./6.md)

[9. String Datatype : String Datatype](./7.md)

[10. Reserved Keywords](./8.md)

[11. Statement's and Comment's](./9.md)

[12. Using Input function](./10.md)

[13. Introducing Boolean Datatype](./11.md)

[14. Condition's : If, ElIf, Else Statements](./12.md)

[15. Exercises](./13.md)

[16. Combining Condition's with Logical Operator's](./14.md)

[17. Variables with No Value—None](./15.md)

[18. The Difference Between Strings and Numbers](./16.md)

[19. Introduction to Loop's](./17.md)

[20. Break, Continue, For else, While else](./18.md)

[21. Exercises](./20.md)

[21.a. Lets draw a flag](./35.md)

[22. Collections: Python Lists](./19.md)

[23. Collections: List Exercise](./19E.md)

[24. Collections: Tuple](./21.md)

[23. Collections: Tuple Exercise](./21E.md)

[25. Collections: Python Sets](./22.md)

[23. Collections: Set Exercise](./22E.md)

[26. Collections: Python Dictionary](./23.md)

[23. Collections: Dictionary Exercise](./23E.md)

[27. Collections: Exercises](./24.md)

[28. Python Functions](./25.md)

[29. Python Modules](./26.md)

[30. How to Use Classes and Objects](./27.md)

[32. Python Builtin Functions](./28.md)

[31. Working with files](./29.md)

[33. Useful Python Modules](./30.md)

[34. Deep down turtle Module](./31.md)

[35. Recursive Drawing](./32.md)

[36. Project using Pygame Module](./33.md)

[38. Doing Self Study](./34.md)











