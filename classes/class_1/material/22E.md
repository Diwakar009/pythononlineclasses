# Collection's : Sets Exercise

1) create a empty set named sports_set

2) check type of the sports_set.

3) Reassign the bellow items to sports_set

        Football
        Cricket
        Badminton
        Tennis
        Table Tennis
        
 4) Check how many sports names in the sports_set.
 
 5) Add more items one by one to the sports_set
 
        Golf
        Tennis
        Badminton
        Kabadi
 
 6) Check how many sports names in the sports_set.       
        
 7) Update more items to the sports_set using update method
 
        Running
        Koko
        Swimming
        Cycling
        
 8) Remove a below item from the sports_set set individually.
  
        Kabadi
        Koko
        
 9) Try to access the sports_set value using index. like sports_set[0]
         
 10) Iterate and display all the items in the sports_set
 
           








