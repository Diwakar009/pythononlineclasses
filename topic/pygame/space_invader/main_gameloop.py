import pygame

"""
Only Game Loop

1) Initialize Pygame
2) Setting caption and game icon 
3) Create Game Loop and updating the game area

"""
""" Initialization and setup. """
# Initialize the pygame module to kick start.
pygame.init()

# Create the screen - (width, height) - x axis is width and y axis is height and (0,0) is the top left most corner
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Space Invaders")

# load the game icon and set it (32 x 32).
icon = pygame.image.load("spaceship_32_pixel.png")
pygame.display.set_icon(icon)

""" Game Loop """

# Game Loop
running = True
while running:
    """ Change the back ground color by setting RGB - Red , Green , Blue (Max value is 255) """
    screen.fill((0, 0, 0))

    """ Loop the events """
    for event in pygame.event.get():
        """ When event type is quit then the window exits gracefully """
        if event.type == pygame.QUIT:
            running = False

    # Keep updating the screen to update the screen
    pygame.display.update()

# Quit pygame
pygame.quit()
