import pygame

"""
Day1 - Step's

1) Initialize Pygame
2) Setting caption and game icon 
3) Create Game Loop and updating the game area
4) Add Events to make game area stable
5) Add key press and key release events.
6) Download Player image and load it to the game.
7) Move the player left and right when key is pressed. 

"""
""" Initialization and setup. """
# Initialize the pygame module to kick start.
pygame.init()

# Create the screen - (width, height) - x axis is width and y axis is height and (0,0) is the top left most corner
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Space Invaders")

# load the game icon and set it (32 x 32).
icon = pygame.image.load("spaceship_32_pixel.png")
pygame.display.set_icon(icon)

# Load the player image and initialize the position of the player
player_image = pygame.image.load("player_64_pixel.png")
playerX = 370
playerY = 480
playerX_change = 0

""" Define function's """


def show_player(x, y):
    """Show's the player in the screen in the given x and y coordinates"""
    # blit is a function to display image in the screen
    screen.blit(player_image, (x, y))


""" Game Loop """

# Game Loop
running = True
while running:
    """ Change the back ground color by setting RGB - Red , Green , Blue (Max value is 255) """
    screen.fill((0, 0, 0))

    """ Loop the events """
    for event in pygame.event.get():
        """ When event type is quit then the window exits gracefully """
        if event.type == pygame.QUIT:
            running = False

        """ When event key stroke is left key and right key, change the """
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -0.3

            if event.key == pygame.K_RIGHT:
                playerX_change = 0.3

        """ When Key Released """
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0

    """ Show Player Start """
    playerX += playerX_change

    """ Condition for the boundary so that the player don't go beyond the screen """
    if playerX <= 0:
        playerX = 0
    elif playerX >= 736:  # 736 is because taking 64 bit pixels.
        playerX = 736

    show_player(playerX, playerY)
    """ Show Player End """

    # Keep updating the screen to update the screen
    pygame.display.update()

# Quit pygame
pygame.quit()
