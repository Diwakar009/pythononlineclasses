import pygame
import random
import math
from pygame import mixer

# Initialize the pygame module to kick start.
pygame.init()

# Create the screen - (width, height) - x axis is width and y axis is height and (0,0) is the top left most corner
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Space Invaders")

# load the game icon and set it (32 x 32).
icon = pygame.image.load("spaceship_32_pixel.png")
pygame.display.set_icon(icon)

# Load Background
background_image = pygame.image.load("bg_800x600.png")

# Background sound
sound_mute = True

mixer.music.load("background.wav")
if not sound_mute:
    mixer.music.play(-1)  # -1 is for continuous music

# Load the player image and initialize the position of the player
player_image = pygame.image.load("player_64_pixel.png")
playerX = 370
playerY = 480
playerX_change = 0

# Load the enemy image and initialize it in any random postion (re spawning)
enemy_image = pygame.image.load("enemy_64_pixel.png")
enemyX = random.randint(0, 735)  # 370 , 800
enemyY = random.randint(50, 150)  # 50
# enemyX_change = 0.3
enemyX_change = 4
enemyY_change = 40

# Load the bullet Image
# Ready - You can't see the bullet on the screen
# Fire - The bullet is currently moving
bullet_image = pygame.image.load("bullet_32_pixel.png")
bulletX = 0
bulletY = 480
# enemyX_change = 0.3
bulletX_change = 0
bulletY_change = 10
bullet_state = "ready"

# Score
# Site to down load font www.dafont.com
score_value = 0
font = pygame.font.Font('freesansbold.ttf', 32)
textX = 10
textY = 10


def show_score(x, y):
    score = font.render("Score :" + str(score_value), True, (255, 255, 255))
    screen.blit(score, (x, y))


# Game over text
over_font = pygame.font.Font('freesansbold.ttf', 64)


def game_over_text():
    gameover = over_font.render("GAME OVER", True, (255, 255, 255))
    screen.blit(gameover, (200, 250))


def show_player(x, y):
    """ Show's the player in the screen in the given x and y coordinates"""
    # blit is a function to display image in the screen
    screen.blit(player_image, (x, y))


def show_enemy(x, y):
    """ Show's the enemy in the screen in the given x and y coordinates"""
    # blit is a function to display image in the screen
    screen.blit(enemy_image, (x, y))


def fire_bullet(x, y):
    global bullet_state
    bullet_state = "fire"
    screen.blit(bullet_image, (x + 16, y + 10))


# Detecting collusion
def isCollision(enemyX, enemyY, bulletX, bulletY):
    # formula of collusion: square root ((x1-x2) ** 2 + (y1-y2) ** 2))
    distance = math.sqrt(math.pow(enemyX - bulletX, 2) + math.pow(enemyY - bulletY, 2))
    if distance < 27:
        return True
    else:
        return False


# Game Loop
running = True
while running:
    # Change the back ground color by setting RGB - Red , Green , Blue (Max value is 255)
    screen.fill((0, 0, 0))

    # Background Image: It will slows down the movement of player and image
    screen.blit(background_image, (0, 0))

    # Loop the events
    for event in pygame.event.get():
        # When event type is quit then the window exits gracefully
        if event.type == pygame.QUIT:
            running = False

        # When event key stroke is left key and right key, change the
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                # playerX_change = -0.3
                playerX_change = -5
            if event.key == pygame.K_RIGHT:
                # playerX_change = 0.3
                playerX_change = 5

            # if Space is pressed then fire bullet
            if event.key == pygame.K_SPACE:
                if bullet_state == "ready":
                    if not sound_mute:
                        mixer.Sound("laser.wav").play()

                    # get the current coordinate of the space ship
                    bulletX = playerX
                    fire_bullet(bulletX, bulletY)

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0

    playerX += playerX_change
    # Condition for the boundary so that the player don't go beyond the screen
    if playerX <= 0:
        playerX = 0
    elif playerX >= 736:  # 736 is because taking 64 bit pixels.
        playerX = 736

    # Game Over
    if enemyY > 200:
        enemyY = 2000
        game_over_text()

    enemyX += enemyX_change
    # Condition for the boundary so that the enemy don't go beyond the screen (bounce the enemy)
    if enemyX <= 0:
        # enemyX_change = 0.3
        enemyX_change = 4
        enemyY += enemyY_change
    elif enemyX >= 736:  # 736 is because taking 64 bit pixels.
        # enemyX_change = -0.3
        enemyX_change = -4
        enemyY += enemyY_change

    # Bullet Movement - just reset the bullet so that multiple bullet can be fired
    if bulletY <= 0:
        bulletY = 480
        bullet_state = "ready"

    if bullet_state == "fire":
        fire_bullet(bulletX, bulletY)
        bulletY -= bulletY_change

    # Detect collision
    collision = isCollision(enemyX, enemyY, bulletX, bulletY)
    if collision:
        if not sound_mute:
            mixer.Sound("explosion.wav").play()

        bulletY = 400
        bullet_state = "ready"
        score_value += 1  # increase the score
        enemyX = random.randint(0, 735)
        enemyY = random.randint(50, 150)

    # Show the player and enemy in the screen
    show_player(playerX, playerY)
    show_score(textX, textY)
    show_enemy(enemyX, enemyY)

    # Keep updating the screen to update the screen
    pygame.display.update()

# Quit pygame
pygame.quit()
