from turtle import *

colour = input('What colour is your flower? ')
fillcolor(colour)
begin_fill()
for _ in range(5):
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    left(360 / 5)
end_fill()

done()