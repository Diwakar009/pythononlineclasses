# import the turtle modules
import turtle as t

# Forming the window screen
screen = t.getscreen()

# background color green
screen.bgcolor("green")

# window title Turtle
screen.title("Turtle")


# object color
t.color("orange")


# forming front rectangle face
for i in range(2):
    t.forward(100)
    t.left(90)
    t.forward(150)
    t.left(90)

# bottom left side
t.goto(50, 50)

# forming back rectangle face
for i in range(2):
    t.forward(100)
    t.left(90)
    t.forward(150)
    t.left(90)

# bottom right side
t.goto(150, 50)
t.goto(100, 0)

# top right side
t.goto(100, 150)
t.goto(150, 200)

# top left side
t.goto(50, 200)
t.goto(0, 150)

t.done()