import turtle as t

"""India flag"""
window = t.getscreen()

t.shape("triangle")

length = 100
width = 75
angle = 90

""" Outer rectangle """
t.color("black")
t.forward(length)
t.left(angle)
t.forward(width)
t.left(angle)
t.forward(length)
t.left(angle)
t.forward(width)
t.left(angle)
t.forward(length)

"""Bring the turtle home"""
t.home()

t.color("green")
t.begin_fill()
t.forward(length)
t.left(angle)
t.forward(width / 3)
t.left(angle)
t.forward(length)
t.left(angle)
t.forward(width / 3)
t.left(angle)
t.forward(length)
t.end_fill()

t.penup()
t.backward(length)
t.left(90)
t.forward((width / 3) * 2)
t.right(90)
t.pendown()

t.color("orange")
t.begin_fill()
t.forward(length)
t.left(angle)
t.forward(width / 3)
t.left(angle)
t.forward(length)
t.left(angle)
t.forward(width / 3)
t.left(angle)
t.forward(length)
t.end_fill()

# Go to Middle of the rectangle
t.penup()
t.backward(length / 2)
t.right(90)
t.forward((width / 3) / 2)
t.right(90)
t.forward((width / 3) / 2)
t.left(90)
t.pendown()

# Wheel
t.color("black")
t.circle((width / 3) / 2)

t.penup()
t.left(90)
t.forward((width / 3) / 2)
t.pendown()

for i in range(36):
    t.forward(((width / 3) / 2))
    t.backward(((width / 3) / 2))
    t.left(10)

t.hideturtle()
window.exitonclick()












