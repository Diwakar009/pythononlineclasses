import turtle as t

s = t.getscreen()

length = 450
width = 250
gap = 25
s.bgcolor("black")
# Rectangle
t.color("red")
t.begin_fill()
for i in range(2):
    t.forward(length)
    t.left(90)
    t.forward(width)
    t.left(90)
t.end_fill()
t.home()

t.color("white")
for _ in range(2):
    t.begin_fill()
    for i in range(2):
        t.forward((length / 2) - gap)
        t.left(90)
        t.forward((width / 2) - gap)
        t.left(90)
    t.end_fill()

    t.penup()
    t.forward((length / 2) + gap)
    t.pendown()

    t.begin_fill()
    for i in range(2):
        t.forward((length / 2) - gap)
        t.left(90)
        t.forward((width / 2) - gap)
        t.left(90)
    t.end_fill()

    t.penup()
    t.home()
    t.left(90)
    t.forward((width / 2) + gap)
    t.right(90)
    t.pendown()

t.hideturtle()
t.done()

