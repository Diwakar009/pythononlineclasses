import turtle as t

s = t.getscreen()

s.bgcolor("light green")
t.shape("turtle")
t.forward(300)
t.left(90)
t.forward(100)
t.left(90)
t.forward(300)
t.left(90)
t.forward(100)

t.done()
