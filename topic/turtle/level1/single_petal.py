import turtle

# Try making a flower

turtle.up()
turtle.goto(-100,-100)
turtle.down()
turtle.fillcolor('yellow')
turtle.begin_fill()
turtle.circle(100,70)
turtle.left(110)
turtle.circle(100,70)
turtle.end_fill()

turtle.done()