# import the turtle modules
import turtle as t

# Forming the window screen
screen = t.getscreen()

# background color green
screen.bgcolor("green")

# window title Turtle
screen.title("Turtle")

# object color
t.color("orange")

# forming front square face
for i in range(4):
    t.forward(100)
    t.left(90)

# bottom left side
t.goto(50, 50)

# forming back square face
for i in range(4):
    t.forward(100)
    t.left(90)

# bottom right side
t.goto(150, 50)
t.goto(100, 0)

# top right side
t.goto(100, 100)
t.goto(150, 150)

# top left side
t.goto(50, 150)
t.goto(0, 100)

t.done()