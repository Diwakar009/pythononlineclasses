import turtle as t

""" Wait for user input from keyboard for options """

print("Which shape you want to draw in the canvas:")
print("R. Rectangle")
print("C. Circle")
print("S. Square")
print("I. Circle Spiral")
print("T. Triangle")


option = input("Enter your option : ")

s = t.getscreen()
t.shape("turtle")

if option == 'R':
    length = int(input("Please enter length of the rectangle : "))
    breath = int(input("Please enter breath of the rectangle : "))
    angle = 90
    s.bgcolor("light green")
    t.forward(breath)
    t.left(angle)
    t.forward(length)
    t.left(angle)
    t.forward(breath)
    t.left(angle)
    t.forward(length)
elif option == 'C':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.circle(radius)
elif option == 'I':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.color("green")
    t.speed(0)
    for _ in range(36):
        t.circle(radius)
        t.left(10)
elif option == 'S':
    side = int(input("Please enter the length of a square side : "))
    angle = 90
    s.bgcolor("light pink")
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)
else:
    s.bgcolor("orange")
    t.write("Nothing is draw...", font=("Arial", 16, "normal"), align="center")

t.hideturtle()
t.done()