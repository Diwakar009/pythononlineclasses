import turtle as t

s = t.getscreen()
s.bgcolor("light green")
t.hideturtle()
t.color('deep pink')
style = ('Courier', 25, 'italic')

statement = 'My Name is Tanisha, and I am 12 year\'s Old'
address = "21 - House Number, New Jersey, United States of America"

t.write(statement, font=style, align='center')
t.penup()
t.goto(-130, -130)
t.write(address, font=style, align='center')
t.done()
