import turtle as t

""" Draws a rectangle """

s = t.getscreen()

""" 
Declare variables so that it can be reused in the all the place's it is needed
"""
length = 100
width = 300
angle = 90

s.bgcolor("light green")
t.shape("turtle")
t.forward(width)
t.left(angle)
t.forward(length)
t.left(angle)
t.forward(width)
t.left(angle)
t.forward(length)

t.done()
