import turtle as t
""" Draws a rectangle """
print("To draw a rectangle: ")
s = t.getscreen()
""" Wait for user input from keyboard for length and width """

length = int(input("Please enter length of the rectangle: "))
width = int(input("Please enter width of the rectangle: "))
angle = 90

s.bgcolor("light green")
t.shape("turtle")
t.forward(width)
t.left(angle)
t.forward(length)
t.left(angle)
t.forward(width)
t.left(angle)
t.forward(length)
t.done()
