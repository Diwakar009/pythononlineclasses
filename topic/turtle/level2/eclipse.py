import turtle

import math

screen = turtle.Screen()
screen.setup(1000,1000)
screen.title("Oval by Center - PythonTurtle.Academy")
turtle.speed(0)
turtle.hideturtle()

def draw_oval(x,y,radius_red,radius_blue,tilt):
    turtle.up()
    turtle.goto(x,y)
    turtle.seth(90+tilt)
    turtle.fd((radius_red-radius_blue)*math.sqrt(2)/2)
    turtle.left(135)
    turtle.fd(radius_red)
    turtle.down()
    turtle.seth(-45+tilt)
    turtle.circle(radius_red,90,100)
    turtle.circle(radius_blue,90,100)
    turtle.circle(radius_red,90,100)
    turtle.circle(radius_blue,90,100)

r1 = 500
r2 = 250
tilt = 0
for i in range(15):
    draw_oval(0,0,r1,r2,tilt)
    r1 *= 0.7
    r2 *= 0.7
    # tilt += 45

turtle.done()