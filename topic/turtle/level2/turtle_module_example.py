import turtle as t
import shape as sh

""" Wait for user input from keyboard for options """

print("Which shape you want to draw in the canvas:")
print("R. Rectangle")
print("C. Circle")
print("S. Square")
print("I. Circle Spiral")
print("T. Triangle")

option = input("Enter your option : ")

s = t.getscreen()
t.shape("turtle")

if option == 'R':
    length = int(input("Please enter length of the rectangle : "))
    breath = int(input("Please enter breath of the rectangle : "))
    angle = 90
    s.bgcolor("light green")
    sh.draw_rectangele(t, length, breath)

elif option == 'C':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.circle(radius)
elif option == 'I':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.color("green")
    t.speed(0)
    sh.draw_spiralCircle(t, 100)
    t.forward(200)
    sh.draw_spiralCircle(t, 100)
elif option == 'S':
    side = int(input("Please enter the length of a square side : "))
    angle = 90
    s.bgcolor("light pink")
    sh.draw_square(t, side)
else:
    s.bgcolor("orange")
    t.write("Nothing is draw...", font=("Arial", 16, "normal"), align="center")

t.hideturtle()
t.done()
