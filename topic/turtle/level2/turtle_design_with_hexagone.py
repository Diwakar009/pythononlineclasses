import turtle as t


def hexagone():
    t.forward(200)
    t.right(61)
    t.forward(200)
    t.right(61)
    t.forward(200)
    t.right(61)
    t.forward(200)
    t.right(61)
    t.forward(200)
    t.right(61)
    t.forward(200)
    t.right(61)


t.speed(0)
t.pencolor('blue')
t.bgcolor('black')

x = 0
t.penup()

t.right(45)
t.forward(90)
t.right(135)

t.down()
while x < 120:
    hexagone()
    t.right(11.1111)
    x = x + 1

t.done()
