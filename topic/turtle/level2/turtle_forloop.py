from turtle import *
import random

screen = getscreen()
screen.title("Marking an area")
screen.setup(500,500,0,0)
screen.bgcolor("orange")

delay(0)

for x in range(1,200):
    xPos = random.randint(-250,250)
    yPos = random.randint(-250,250)
    goto(xPos,yPos)
    dot(20,"blue")

done()



