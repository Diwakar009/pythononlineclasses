import turtle as t

# VIBGYOR
rainbow_colors = ("violet", "indigo", "blue", "green", "yellow", "orange", "red")

screen = t.getscreen()
screen.title("Rainbow Using Tuples")
screen.bgcolor("black")

# screen.setup(width=1.0, height=1.0)  # for full screen
screen.setup(width=1000, height=1000)
t.color("white")
radius = 100
difference = 0
line_width = 50

# Take turtle to the start of the screen to draw the semi circle
t.penup()
t.right(180)
t.forward(radius)
t.right(90)
t.pendown()

t.width(line_width)

for rainbow_color in rainbow_colors:  # iterate the tuple
    t.color(rainbow_color)
    t.circle(- (radius + difference), 180)

    t.penup()
    t.right(90)
    t.forward(radius + difference)  # get to the center
    difference = difference + line_width
    t.forward((radius + difference))
    t.right(90)
    t.pendown()

t.hideturtle()
t.done()
