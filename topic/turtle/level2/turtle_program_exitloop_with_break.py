import turtle as t


def square(side_length):
    for _ in range(4):
        t.forward(side_length)
        t.left(90)


t.hideturtle()
t.color("red")
t.speed(0)
i = 0
while True:
    if i > 500:
        break
    square(i)
    t.right(6)
    i += 2

t.done()
