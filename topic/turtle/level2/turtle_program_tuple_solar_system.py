import turtle as t

screen = t.getscreen()
screen.bgcolor("black")
screen.setup(width=1.0, height=1.0)  # for full screen

# nested tuple (Sun, (List of 9 Planets))

solar_system = (
    ("Sun", 250, "#FCD440"),  # Sun Yellow
    [
        ("Mercury", 50, "#504E51", 0),  # Gray
        ("Venus", 75, "#DDD8D4", 0),  # Timberwolf
        ("Earth", 65, "#006994", 1), # Sea Blue
        ("Mars", 100, "red", 2),
        ("Jupiter", 175, "#ffac3c", 63),  # Yellowish Orange
        ("Saturn", 160, "brown", 60),
        ("Uranus", 150, "light blue", 27),
        ("Neptune", 125, "blue", 13),
        ("Pluto", 20, "light yellow", 3)
    ])


star, planets = solar_system  # unpacking

star_name, star_radius, star_color = star  # un packing

t.penup()
t.goto(800, 400)
t.pendown()
t.left(180)
t.dot(star_radius, star_color)
t.penup()
t.color("white")
t.write(star_name, align="center", font=("Arial", 16, "bold"))
t.left(25)
t.forward(star_radius / 2)
t.pendown()

for planet in planets:  # Tuple iteration
    planet_name, planet_radius, planet_color, planet_satelite = planet  # Unpacking

    t.penup()
    t.forward(planet_radius)
    t.right(90)
    t.forward(planet_radius)
    t.write(planet_name + "(" + str(planet_satelite) + ")", align="center", font=("Arial", 16, "bold"))

    t.right(180)
    t.forward(planet_radius)
    t.right(90)
    t.pendown()

    t.dot(planet_radius, planet_color)

    if planet_name == 'Saturn':
        # Draw a eclipse around the planet
        pass

    t.penup()
    t.forward(80)
    t.pendown()

t.done()
