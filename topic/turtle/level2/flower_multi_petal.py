from turtle import *

outerpetals = 10
innerpetals = 8
outercolour = "darkgreen"
incolour="pink"

inpetals_angle = 360 / innerpetals
outpetals_angle = 360 / outerpetals
pensize(4)
pencolor(outercolour)
fillcolor(outercolour)

shape("turtle")

for _ in range(int(outerpetals)):
    begin_fill()
    for _ in range(2):
        forward(60)
        left(45)
        forward(60)
        left(135)
    end_fill()
    left(outpetals_angle)
pencolor(incolour)
fillcolor(incolour)

for _ in range(int(innerpetals)):
    begin_fill()
    for _ in range(4):
        forward(50)
        left(90)
    end_fill()
    left(inpetals_angle)

done()
