from turtle import Turtle, Screen

COORDINATES = (-2.0, -1.0, 1.0, 1.0)

WIDTH, HEIGHT = 320, 240

colors = {i: ((i & 0b1111) << 4, (i & 0b111) << 5, (i & 0b11111) << 3) for i in range(2 ** 5)}

def mandelbrot(turtle, minimum_x, minimum_y, maximum_x, maximum_y, iterations, width, height):

    step_x, step_y = (maximum_x - minimum_x) / width, (maximum_y - minimum_y) / height

    real = minimum_x

    current_color = 0

    while real < maximum_x:

        imaginary = minimum_y

        while imaginary < maximum_y:

            color = 0

            c, z = complex(real, imaginary), 0j

            for i in range(iterations):
                if abs(z) >= 4.0:
                    color = i & 31
                    break

                z = z * z + c

            if color != current_color:
                turtle.color(colors[color])
                current_color = color

            turtle.setpos(real, imaginary)
            turtle.stamp()
            imaginary += step_y

        screen.update()
        real += step_x

screen = Screen()
screen.setup(WIDTH, HEIGHT)
screen.setworldcoordinates(*COORDINATES)
screen.colormode(255)
screen.tracer(0, 0)

turtle = Turtle(visible=False)
turtle.speed("fastest")
turtle.setundobuffer(1)  # unfortunately setundobuffer(None|0) broken for turtle.stamp()

turtle.begin_poly()
# Yes, this is an empty polygon but produces a dot -- I don't know if this works on all platforms
turtle.end_poly()

screen.register_shape("pixel", turtle.get_poly())
turtle.shape("pixel")
turtle.up()

mandelbrot(turtle, *COORDINATES, 31, WIDTH, HEIGHT)

screen.exitonclick()