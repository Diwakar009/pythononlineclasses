import turtle

window = turtle.Screen()
t = turtle.Turtle()

my_list = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120]

for value in my_list:
    t.forward(value)
    t.stamp()
    t.right(30)