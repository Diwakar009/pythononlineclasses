import turtle as t

screen = t.getscreen()
screen.bgcolor("black")
screen.setup(width=1.0, height=1.0)  # for full screen

solar_system = ("Sun", ("Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"))
solar_system_dict = {
    "Sun": {
        "size": 200,
        "color": "Orange",
        "diameter": "1.3927 million km",
        "surface-temperature": "5,505 Degree Celsius"
    },
    "Mercury": {
        "size": 50,
        "color": "Dark Gray",
        "satellite": 0,
        "diameter": "4879.4 km",
        "day-temperature": "430 Degrees Celsius",
        "night-temperature": "-180 Degrees Celsius",
        "distance-from-star": "46.766 million Km",
        "rotation-around-star": "88 Earth Days"
    },
    "Venus": {
        "size": 75,
        "color": "White",
        "satellite": 0,
        "diameter": "12,104 km",
        "day-temperature": "460 Degrees Celsius",
        "night-temperature": "460 Degrees Celsius",
        "distance-from-star": "108.8 million Km",
        "rotation-around-star": "224.7 Earth Days"
    },
    "Earth": {
        "size": 100,
        "color": "Blue",
        "satellite": 1,
        "diameter": "12,742 km",
        "day-temperature": "70.7 Degrees Celsius",
        "night-temperature": "-89.2 Degrees Celsius",
        "distance-from-star": "151.81 million Km",
        "rotation-around-star": "365.26 Earth Days"
    },
    "Mars": {
        "size": 65,
        "color": "Red",
        "satellite": 2,
        "diameter": "6,779 km",
        "day-temperature": "20 Degrees Celsius",
        "night-temperature": "-73 Degrees Celsius",
        "distance-from-star": "207.87 million Km",
        "rotation-around-star": "686.93 Earth Days"
    },
    "Jupiter": {
        "size": 115,
        "color": "Orange",
        "satellite": 63,
        "diameter": "1,39,820 km",
        "day-temperature": "-145 Degrees Celsius",
        "night-temperature": "-145 Degrees Celsius",
        "distance-from-star": "770.75 million Km",
        "rotation-around-star": "4,330.6 Earth Days"
    },
    "Saturn": {
        "size": 135,
        "color": "Brown",
        "satellite": 60,
        "diameter": "116,460 km",
        "day-temperature": "-185 Degrees Celsius",
        "night-temperature": "-185 Degrees Celsius",
        "distance-from-star": "1.4945 Billion Km",
        "rotation-around-star": "10,759 Earth Days"
    },
    "Uranus": {
        "size": 125,
        "color": "Light Blue",
        "satellite": 27,
        "diameter": "50,724 km",
        "day-temperature": "-197 Degrees Celsius",
        "night-temperature": "-197 Degrees Celsius",
        "distance-from-star": "2.9607 Billion Km",
        "rotation-around-star": "30,687 Earth Days"
    },
    "Neptune": {
        "size": 100,
        "color": "Blue",
        "satellite": 13,
        "diameter": "49,244 km",
        "day-temperature": "-200 Degrees Celsius",
        "night-temperature": "-200 Degrees Celsius",
        "distance-from-star": "4.4765 Billion Km",
        "rotation-around-star": "60,190 Earth Days"
    },
    "Pluto": {
        "size": 30,
        "color": "Light Yellow",
        "satellite": 3,
        "diameter": "2376.6 km",
        "day-temperature": "-240 Degrees Celsius",
        "night-temperature": "-226 Degrees Celsius",
        "distance-from-star": "5.9 Billion Km",
        "rotation-around-star": "8,680 Earth Days"
    }
}

text_display_position = (250, 100)
star, planets = solar_system  # unpacking
star_dict = solar_system_dict.get(star)  # un packing
star_radius = star_dict.get("size") / 2
star_color = star_dict.get("color")
star_name = star

star_characteristics = 'Name - ' + star_name + '\n'
star_characteristics = star_characteristics + 'Color = ' + star_dict.get("color") + '\n'
star_characteristics = star_characteristics + 'Diameter = ' + star_dict.get("diameter") + '\n'
star_characteristics = star_characteristics + 'Temperature = ' + star_dict.get("surface-temperature")

counter = 1
t.color("red")
t.penup()
t.goto(555, -100)
t.right(90)
t.forward(text_display_position[1])
t.pendown()
t.color(star_color)
t.write(star_characteristics, align="center", font=("Arial", 10, "bold"))
t.penup()
t.right(180)
t.forward(text_display_position[1])
t.right(90)
t.pendown()

t.left(180)
t.dot(star_radius, star_color)
t.penup()
t.forward(star_radius / 2)
t.pendown()

for planet in planets:  # Tuple planet iteration

    planet_dict = solar_system_dict.get(planet)
    planet_name = planet
    planet_radius = planet_dict.get("size") / 2
    planet_color = planet_dict.get("color")

    t.color(planet_color)

    characteristics = 'Name = ' + planet_name + '\n'
    characteristics = characteristics + 'Color = ' + planet_dict.get("color") + '\n'
    characteristics = characteristics + 'Diameter = ' + planet_dict.get("diameter") + '\n'
    characteristics = characteristics + 'Satellite = ' + str(planet_dict.get("satellite")) + '\n'
    characteristics = characteristics + 'Day Temp. = ' + planet_dict.get("day-temperature") + '\n'
    characteristics = characteristics + 'Night Temp. = ' + planet_dict.get("night-temperature") + '\n'
    characteristics = characteristics + 'Dist. from star = ' + planet_dict.get("distance-from-star") + '\n'
    characteristics = characteristics + 'Rotation around star = ' + planet_dict.get("rotation-around-star") + '\n'

    t.penup()
    t.forward(planet_radius)
    t.right(90)
    t.forward(text_display_position[counter % 2])
    t.write(characteristics, align="center", font=("Arial", 9, "normal"))

    t.right(180)
    t.forward(text_display_position[counter % 2])
    t.right(90)
    t.pendown()

    t.dot(planet_radius, planet_color)

    t.penup()
    t.forward(80)
    t.pendown()
    counter = counter + 1

t.done()
