import turtle as t

# tuple
shapes = ('arrow','turtle','circle','square','triangle','classic')

s = t.getscreen()
t.speed(100)
for shape in shapes:
    t.shape(shape)
    t.stamp()
    t.forward(50)

t.done()