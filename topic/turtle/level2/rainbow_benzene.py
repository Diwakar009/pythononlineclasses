import turtle
colors = ('red', 'purple', 'blue', 'green', 'orange', 'yellow')
t = turtle.Pen()
turtle.bgcolor('black')
for x in range(360):
    color_index = x % 6
    t.pencolor(colors[color_index])
    t.width(x/100 + 1)
    t.forward(x)
    t.left(59)