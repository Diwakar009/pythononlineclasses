import turtle as t

s = t.getscreen()
s.bgcolor("black")
my_list = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140]
t.color("red")

for value in my_list:
    t.forward(value)
    t.stamp()
    t.backward(value)
    t.right(30)

t.done()
