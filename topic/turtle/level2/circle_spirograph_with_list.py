import turtle as t
s = t.getscreen()
s.bgcolor("black")
t.speed(0)
t.width(2)

for i in range(6):
    for selected_color in ["red","magenta","blue","cyan","green","yellow","white"]:
        t.color(selected_color)
        t.circle(100)
        t.left(10)

t.done()
