# Module for the drawing turtle shapes

def draw_rectangele(t,length,breadth):
    """ Draws a Rectangle """
    angle = 90
    t.forward(breath)
    t.left(angle)
    t.forward(length)
    t.left(angle)
    t.forward(breath)
    t.left(angle)
    t.forward(length)

def draw_square(t,side):
    angle = 90
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)

def draw_spiralCircle(t,radius):
    for _ in range(36):
        t.circle(radius)
        t.left(10)
