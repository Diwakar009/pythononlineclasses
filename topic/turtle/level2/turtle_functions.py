import turtle as t


def draw_rectangele(length, breadth):
    angle = 90
    t.forward(breath)
    t.left(angle)
    t.forward(length)
    t.left(angle)
    t.forward(breath)
    t.left(angle)
    t.forward(length)


def draw_square(side):
    angle = 90
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)
    t.left(angle)
    t.forward(side)


def draw_spiralCircle(radius):
    for _ in range(36):
        t.circle(radius)
        t.left(10)


""" Wait for user input from keyboard for options """

print("Which shape you want to draw in the canvas:")
print("R. Rectangle")
print("C. Circle")
print("S. Square")
print("I. Circle Spiral")
print("T. Triangle")

option = input("Enter your option : ")

s = t.getscreen()
t.shape("turtle")

if option == 'R':
    length = int(input("Please enter length of the rectangle : "))
    breath = int(input("Please enter breath of the rectangle : "))
    angle = 90
    s.bgcolor("light green")
    draw_rectangele(length, breath)

elif option == 'C':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.circle(radius)
elif option == 'I':
    radius = int(input("Please enter the radius of the circle : "))
    s.bgcolor("light blue")
    t.color("green")
    t.speed(0)
    draw_spiralCircle(100)
    t.forward(200)
    draw_spiralCircle(100)
elif option == 'S':
    side = int(input("Please enter the length of a square side : "))
    angle = 90
    s.bgcolor("light pink")
    draw_square(side)
else:
    s.bgcolor("orange")
    t.write("Nothing is draw...", font=("Arial", 16, "normal"), align="center")

t.hideturtle()
t.done()
