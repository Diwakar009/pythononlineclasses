import turtle as t

a = 5
while a < 200:
    t.forward(a)
    t.right(90)
    a = a + 2

t.exitonclick()

