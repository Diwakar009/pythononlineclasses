import turtle as t

from random import *

size = randint(3, 10)


def draw_star(size):
    t.color("white", "white")
    t.begin_fill()
    for i in range(36):
        t.forward(size)
        t.left(170)
    t.end_fill()


t.speed(0)

s = t.getscreen()
s.bgcolor("black")
for j in range(25):
    x = randint(-675, 600)
    y = randint(-300, 350)
    draw_star(size)
    t.penup()
    t.goto(x, y)
    t.pendown()
