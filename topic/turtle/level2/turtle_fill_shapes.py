import turtle
import random

colors = ["red", "blue", "green", "purple", "yellow", "orange", "black"]

tim = turtle.Turtle()
tim.color("red", "blue")


for x in range(10):
    randColor = random.randrange(0, len(colors))
    rand1 = random.randrange(-300, 300)
    rand2 = random.randrange(-300, 300)

    tim.color(colors[randColor], colors[random.randrange(0, len(colors))])

    tim.penup()
    tim.setpos((rand1, rand2))
    tim.pendown()

    tim.begin_fill()
    tim.circle(random.randrange(0, 80))
    tim.end_fill()

turtle.done()