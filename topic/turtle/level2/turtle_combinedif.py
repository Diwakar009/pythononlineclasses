from turtle import *
import random

screen = getscreen()
screen.title("Marking an area")
screen.setup(500, 500, 0, 0)
screen.bgcolor("orange")

delay(0)

for x in range(1, 200):
    xPos = random.randint(-250, 250)
    yPos = random.randint(-250, 250)
    goto(xPos, yPos)
    if xPos > 50 and xPos < 200 and yPos > 50 and yPos < 200:
        dot(20, "blue")
    else:
        dot(20,"green")

done()
