import turtle

screen = turtle.Screen()
screen.setup(500,500)
screen.title("Oval with 4 Arcs - PythonTurtle.Academy")
turtle.speed(1)
turtle.shape('turtle')

turtle.up()
turtle.goto(-140,-75)
turtle.down()
turtle.seth(-45)
turtle.color('red')
turtle.circle(25,90)
turtle.color('blue')
turtle.circle(25 / 2,90)
turtle.color('red')
turtle.circle(25,90)
turtle.color('blue')
turtle.circle(25 / 2,90)

turtle.done()