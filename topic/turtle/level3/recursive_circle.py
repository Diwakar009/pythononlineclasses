import turtle as t
import random

window = t.Screen()
t.speed(0)

choose_color = ["blue", "green", "yellow", "orange", "violet"]

growth_factor = 10.2
width = .5
length = 10
max_circles = length * 25


def draw_circle():
    global length
    global width
    if length < max_circles:
        width += .2
        t.width(width)
        t.color(random.choice(choose_color))
        t.circle(length)
        length += growth_factor
        t.penup()
        t.right(90)
        t.forward(growth_factor)
        t.left(90)
        t.pendown()
        draw_circle()


draw_circle()
window.exitonclick()
