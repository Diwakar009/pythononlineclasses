import turtle as t

""""
Adapt the above program to change the color of its three sub-triangles at some depth of recursion. 
The illustration below shows two cases: on the left, the color is changed at depth 0 (the outmost level of recursion), 
on the right, at depth 2. If the user supplies a negative depth, the color never changes. 
(Hint: add a new optional parameter colorChangeDepth (which defaults to -1), and make this one smaller on each recursive subcall. 
Then, in the section of code before you recurse, test whether the parameter is zero, and change color.)
"""


def triangle(t, side):
    for _ in range(3):
        t.forward(side)
        t.left(90 + 30)


def koch(t, order, size, colorChangeDepth):
    if order == 0:  # The base case is just a straight line
        triangle(t, size)
    else:
        # koch(t, order - 1, size, colorChangeDepth)

        if colorChangeDepth == 0:
            t.color("red")

        koch(t, order - 1, size / 2, colorChangeDepth - 1)
        t.penup()
        t.forward(size / 2)
        t.pendown()
        if colorChangeDepth == 0:
            t.color("blue")
        koch(t, order - 1, size / 2, colorChangeDepth - 1)
        t.penup()
        t.left(120)
        t.forward(size / 2)
        t.right(120)
        t.pendown()

        if colorChangeDepth == 0:
            t.color("magenta")

        koch(t, order - 1, size / 2, colorChangeDepth - 1)
        t.penup()
        t.right(120)
        t.forward(size / 2)
        t.left(120)
        t.pendown()


t.getscreen().bgcolor("green")
t.speed(0)
t.width(2)
t.penup()
t.goto(-400, 0)
t.pendown()
koch(t, 4, 400, 0)
t.penup()
t.goto(250, 0)
t.pendown()
koch(t, 4, 400, 2)

t.done()
