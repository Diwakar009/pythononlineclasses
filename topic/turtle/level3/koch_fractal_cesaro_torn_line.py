import turtle as t

"""
Draw a Cesaro torn line fractal, of the order given by the user. We show four different lines of orders 0,1,2,3. 
In this example, the angle of the tear is 10 degrees.
"""


def koch(t, order, size):
    """
       Make turtle t draw a Koch fractal of 'order' and 'size'.
       Leave the turtle facing the same direction.
    """

    if order == 0:  # The base case is just a straight line
        t.forward(size)
    else:
        koch(t, order - 1, size / 3)  # Go 1/3 of the way
        t.right(85)
        koch(t, order - 1, size / 3)
        t.left(170)
        koch(t, order - 1, size / 3)
        t.right(85)
        koch(t, order - 1, size / 3)


t.penup()
t.goto(-500, 0)
t.pendown()
koch(t, 0, 200)

t.penup()
t.goto(-200, 0)
t.pendown()
koch(t, 1, 200)

t.penup()
t.goto(0, 0)
t.pendown()
koch(t, 2, 400)

t.penup()
t.goto(300, 0)
t.pendown()
koch(t, 3, 500)


t.done()
