import turtle as t

"""
Four lines make a square. Use the code in part a) to draw cesaro squares. 
Varying the angle gives interesting effects — experiment a bit, or perhaps let the user input the angle of the tear.
"""


def koch(t, order, size):
    """
       Make turtle t draw a Koch fractal of 'order' and 'size'.
       Leave the turtle facing the same direction.
    """

    if order == 0:  # The base case is just a straight line
        t.forward(size)
    else:
        koch(t, order - 1, size / 3)  # Go 1/3 of the way
        t.right(85)
        koch(t, order - 1, size / 3)
        t.left(170)
        koch(t, order - 1, size / 3)
        t.right(85)
        koch(t, order - 1, size / 3)


def draw_cesaro_square(t, order, size):
    koch(t, order, size)
    t.right(90)
    koch(t, order, size)
    t.right(90)
    koch(t, order, size)
    t.right(90)
    koch(t, order, size)


t.penup()
t.goto(-500, 0)
t.pendown()
# t.left(180)

t.speed(0)

draw_cesaro_square(t, 0, 200)

t.penup()
t.goto(-200, 0)
t.pendown()

draw_cesaro_square(t, 1, 200)

t.penup()
t.goto(300, 0)
t.pendown()

draw_cesaro_square(t, 2, 400)

t.penup()
t.goto(600, 0)
t.pendown()

draw_cesaro_square(t, 3, 500)

t.done()
