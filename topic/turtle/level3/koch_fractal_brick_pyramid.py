import turtle as t


def koch(t, order, size):
    """
       Make turtle t draw a Koch fractal of 'order' and 'size'.
       Leave the turtle facing the same direction.
    """

    if order == 0:  # The base case is just a straight line
        t.forward(size)
    else:
        koch(t, order - 1, size / 2)  # Go 1/3 of the way
        t.left(90)
        koch(t, order - 1, size / 2)
        t.right(90)
        koch(t, order - 1, size / 2)
        t.right(90)
        koch(t, order - 1, size / 2)
        t.left(90)
        koch(t, order - 1, size / 2)


t.penup()
t.goto(-500, -300)
t.pendown()
koch(t, 2, 400)
t.done()
