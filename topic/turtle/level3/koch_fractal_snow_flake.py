import turtle as t

"""
Modify the Koch fractal program so that it draws a Koch snowflake, like this:
"""


def koch(t, order, size):
    """
       Make turtle t draw a Koch fractal of 'order' and 'size'.
       Leave the turtle facing the same direction.
    """

    if order == 0:  # The base case is just a straight line
        t.forward(size)
    else:
        koch(t, order - 1, size / 3)  # Go 1/3 of the way
        t.left(60)
        koch(t, order - 1, size / 3)
        t.right(120)
        koch(t, order - 1, size / 3)
        t.left(60)
        koch(t, order - 1, size / 3)


def snowflake(order,size):
    # t.begin_fill()
    t.left(60)
    koch(t, order, size)
    t.right(120)
    koch(t, order, size)
    t.left(-120)
    koch(t, order, size)
    # t.end_fill()


def draw_snowflake(x_cor, y_cor, size):
    t.penup()
    t.goto(x_cor, y_cor)
    t.pendown()
    snowflake(1,size)


t.speed(0)
s = t.getscreen()
s.bgcolor("black")
t.color("white")
draw_snowflake(-250, 100, 200)
draw_snowflake(250 + 100, 100, 75)

t.done()
