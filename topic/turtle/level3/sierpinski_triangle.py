import turtle as t

""""
A Sierpinski triangle of order 0 is an equilateral triangle. 
An order 1 triangle can be drawn by drawing 3 smaller triangles (shown slightly disconnected here, just to help our understanding). 
Higher order 2 and 3 triangles are also shown. Draw Sierpinski triangles of any order input by the user.

"""


def triangle(t, side):
    t.forward(side)
    t.left(90 + 30)
    t.forward(side)
    t.left(90 + 30)
    t.forward(side)
    t.left(90 + 30)


def koch(t, order, size):
    if order == 0:  # The base case is just a straight line
        triangle(t, size)
    else:
        # koch(t, order - 1, size)  # Go 1/3 of the way
        koch(t, order - 1, size / 2)
        t.penup()
        t.forward(size / 2)
        t.pendown()
        koch(t, order - 1, size / 2)
        t.penup()
        t.left(90 + 30)
        t.forward(size / 2)
        t.right(90 + 30)
        t.pendown()
        koch(t, order - 1, size / 2)
        t.penup()
        t.right(120)
        t.forward(size / 2)
        t.left(90 + 30)
        t.pendown()


t.speed(0)
t.penup()
t.goto(-500, 0)
t.pendown()
start = 0
location = (-500,-300,-100,200,300)
for i in range(5):
    koch(t, i, 100)
    t.penup()
    t.goto(location[i], 0)
    t.pendown()


t.done()
