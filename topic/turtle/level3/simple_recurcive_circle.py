import turtle as t
import random

window = t.Screen()
t.speed(0)

choose_color = ["blue", "green", "yellow", "orange", "violet"]


def draw_circle(radius):
    if radius < 0:
        return
    t.width(5)
    t.color(random.choice(choose_color))
    t.circle(radius)
    radius -= 10
    t.penup()
    t.left(90)
    t.forward(10)
    t.right(90)
    t.pendown()
    draw_circle(radius)


t.penup()
t.goto(0, -250)
t.pendown()
draw_circle(250)
window.exitonclick()
