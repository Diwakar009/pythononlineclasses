# Area module of the different Shapes

def rectangle_area(length, width):
    """ Return's area of rectangle """
    return length * width


def square_area(side):
    """ Return's area of square """
    return side ** 2


def circle_area(radius):
    """ Return's area of circle """
    return 3.14 * (radius ** 2)


def triangle_area(a, b, c):
    """ Return's area of triangle """
    # s = (a+b+c)/2
    # area = √(s(s-a)*(s-b)*(s-c))

    if a < 0 or b < 0 or c < 0 or (a + b <= c) or (a + c <= b) or (b + c <= a):
        print('Not a valid triangle')
        return

    s = (a + b + c) / 2

    # calculate the area
    area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
    return area


def main():
    rect_area = rectangle_area(100, 300)
    expected_output = 30000
    if rect_area != expected_output:
        print("Assertion Error, Expected is %s, Actual is %s" % (expected_output,rect_area))
        raise AssertionError
    else:
        print("Area of rectangle area is %s" % rect_area)


if __name__ == "__main__":
    # execute only if run as a script
    main()
