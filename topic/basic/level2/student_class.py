# Object oriented programming - Defining a class
class Student:

    """ student class, which has student attributes and behaviour"""

    # The instance attributes.
    running = False
    dancing = False

    # Constructor - Initialize the instance attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age

    # Instance methods
    def startRunning(self):
        self.running = True
        print("{} - Started Running".format(self.name))

    def stopRunning(self):
        self.running = False
        print("{} - Stopped Running".format(self.name))

    def startDancing(self):
        self.dancing = True
        print("{} - Started Dancing".format(self.name))

    def stopDancing(self):
        self.dancing = False
        print("{} - Stopped Dancing".format(self.name))

    def getDetails(self):
        details = "Name : {} \n"
        details += "Age : {} \n"
        details += "Activity : {} \n"
        return details.format(self.name, self.age, self.activity())

    def activity(self):
        activity_str = "I am {}."
        if self.dancing:
            return activity_str.format("Dancing")
        elif self.running:
            return activity_str.format("Running")
        else:
            return "I am doing nothing, tell me something to do?"


if __name__ == "__main__":
    tanisha = Student("Tanisha Polaki", 12)
    annya = Student("Annya Kantareddy", 11)
    print(tanisha.getDetails())
    print(annya.getDetails())
    print(annya.activity())
    annya.stopDancing()
    print(annya.activity())
