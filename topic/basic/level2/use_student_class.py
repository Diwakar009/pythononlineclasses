from topic.basic.level2.student_class import Student

vikram = Student("Vikram S", 10)
nikhila = Student("Nikhila S",12)
tanisha = Student("Tanisha P",14)

print(vikram.getDetails())
vikram.startRunning()
print(vikram.activity())

nikhila.startRunning()
print(nikhila.activity())

vikram.stopRunning()

print(vikram.activity())



