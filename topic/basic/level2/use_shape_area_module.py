import shape_area as area


# Area of rectangle

length = 100
width = 300

print("Area of the rectangle is %s " % area.rectangle_area(length, width))


# Area of square

side = 200

print("Area of the square is %s " % area.square_area(side))

# Area of circle

radius = 50

print("Area of the circle is %s " % area.circle_area(radius))

# Area of triangle

a = 5
b = 6
c = 7

print("Area of the triangle is %s " % area.triangle_area(a,b,c))



