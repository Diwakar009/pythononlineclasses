# Program to generate a random number between 0 and 9

# importing the random module
import random

for _ in range(10):
    print(random.randint(0,9))